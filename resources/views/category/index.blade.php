@extends('template')

@section('main')
    <div id="category">
        <h2>Kategori</h2>

        @include('_partial.flash_message')

        @if (count($category) > 0)
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0; ?>
                    @foreach($category as $category)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $category->category }}</td>
                        <td>
                            <div class="box-button">
                                {{ link_to('category/' . $category->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
                            </div>
                            <div class="box-button">
                                {!! Form::open(['method' => 'DELETE', 'action' => ['CategoryController@destroy', $category->id]]) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                {!! Form::close() !!}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>Tidak ada data kategori.</p>
        @endif

        <div class="tombol-nav">
            <a href="category/create" class="btn btn-primary">Tambah Kategori</a>
        </div>

    </div> <!-- / #category -->
@stop