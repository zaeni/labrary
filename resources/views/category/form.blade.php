@if (isset($category))
    {!! Form::hidden('id', $category->id) !!}
@endif

@if ($errors->any())
    <div class="form-group {{ $errors->has('category') ? 'has-error' : 'has-success' }}">
@else
    <div class="form-group">
@endif
    {!! Form::label('category', 'Nama Kategori:', ['class' => 'control-label']) !!}
    {!! Form::text('category', null, ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('category'))
        <span class="help-block">{{ $errors->first('category') }}</span>
    @endif
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>