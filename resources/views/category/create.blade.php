@extends('template')

@section('main')
    <div id="category">
        <h2>Tambah Kategori</h2>

        {!! Form::open(['url' => 'category']) !!}
            @include('category.form', ['submitButtonText' => 'Tambah Kategori'])
        {!! Form::close() !!}
    </div>
@stop