@extends('template')

@section('main')
    <div id="category">
        <h2>Edit Kategori</h2>

        {!! Form::model($category, ['method' => 'PATCH', 'action' => ['CategoryController@update', $category->id]]) !!}
            @include('category.form', ['submitButtonText' => 'Update Kategori'])
        {!! Form::close() !!}
    </div>
@stop