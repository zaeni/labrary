@extends('template')

@section('main')
    <div id="homepage">
        <h2>Daftar Buku</h2>
    </div>
    <br>
    @if (count($books) > 0)
        <div class="row">
            @foreach($books as $book)
                <div class="column">
                    <div class="card">
                        <a href="{{ asset('detail/' . $book->id) }}">
                            <img src="{{ asset('cover_photoupload/' . $book->cover_photo) }}" alt="Cover" style="width:100%">
                            <div class="container">
                                <h4><b>{{ $book->title }}</b></h4>
                                <p>{{ $book->synopsis }}</p>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
    <br>
    <div class="table-nav">
        <div class="jumlah-data">
            <strong> Jumlah Buku: {{ $book_count }} </strong>
        </div>
        <div class="paging">
            {{ $books->links() }}
        </div>
    </div>
@stop