      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1"
                    aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'Labrary') }}</a>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              @if (Auth::check())
                {{-- Book --}}
                @if (Request::segment(1) == 'book')
                  <li class="active"><a href="{{ url('book') }}">Buku <span class="sr-only">(current)</span></a></li>
                @else
                  <li><a href="{{ url('book') }}">Buku</a></li>
                @endif

                {{-- Category --}}
                @if (Request::segment(1) == 'category')
                  <li class="active"><a href="{{ url('category') }}">Kategori <span class="sr-only">(current)</span></a></li>
                @else
                  <li><a href="{{ url('category') }}">Kategori</a></li>
                @endif

                {{-- User --}}
                @if (Request::segment(1) == 'user')
                  <li class="active"><a href="{{ url('user') }}">User <span class="sr-only">(current)</span></a></li>
                @else
                  <li><a href="{{ url('user') }}">User</a></li>
                @endif
              @endif
            </ul>

            {{-- Link Login --}}
            <ul class="nav navbar-nav navbar-right">
              @if (Auth::check())
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              @else
                <li><a href="{{ route('login') }}">Login</a></li>
              @endif
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>