@extends('template')

@section('main')
    <div id="book">
        <h2>Detail Buku</h2>
        <br>
        <table class="table table-striped">
            <tr>
                <th>No. Buku</th>
                <td>{{ $book->book_number }}</td>
            </tr>
            <tr>
                <th>Judul</th>
                <td>{{ $book->title }}</td>
            </tr>
            <tr>
                <th>Kategori</th>
                <td>{{ $book->category->category }}</td>
            </tr>
            <tr>
                <th>Penerbit</th>
                <td>{{ $book->publisher }}</td>
            </tr>
            <tr>
                <th>Tanggal Terbit</th>
                <td>{{ $book->published_date }}</td>
            </tr>
            <tr>
                <th>Foto</th>
                <td>
                   @if (isset($book->cover_photo))
                       <img src="{{ asset('cover_photoupload/' . $book->cover_photo) }}">
                   @else
                        @if ($book->jenis_kelamin == 'L')
                            <img src="{{ asset('cover_photoupload/dummymale.jpg') }}">
                        @else
                            <img src="{{ asset('cover_photoupload/dummyfemale.jpg') }}">
                        @endif
                   @endif
                </td>
            </tr>
        </table>
    </div>
@stop