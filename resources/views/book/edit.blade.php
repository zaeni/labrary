@extends('template')

@section('main')
    <div id="book">
        <h2>Edit Buku</h2>

        {!! Form::model($book, ['method' => 'PATCH', 'action' => ['BookController@update', $book->id], 'files' => true]) !!}
            @include('book.form', ['submitButtonText' => 'Update Buku'])
        {!! Form::close() !!}
    </div>
@stop