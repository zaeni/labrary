@if (isset($book))
    {!! Form::hidden('id', $book->id) !!}
@endif

@if ($errors->any())
    <div class="form-group {{ $errors->has('book_number') ? 'has-error' : 'has-success' }}">
@else
    <div class="form-group">
@endif
    {!! Form::label('book_number', 'No. Buku:', ['class' => 'control-label']) !!}
    {!! Form::text('book_number', null, ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('book_number'))
        <span class="help-block">{{ $errors->first('book_number') }}</span>
    @endif
</div>

@if ($errors->any())
    <div class="form-group {{ $errors->has('title') ? 'has-error' : 'has-success' }}">
@else
    <div class="form-group">
@endif
    {!! Form::label('title', 'Judul:', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('title'))
        <span class="help-block">{{ $errors->first('title') }}</span>
    @endif
</div>

<!-- Category -->
@if ($errors->any())
    <div class="form-group {{ $errors->has('category_id') ? 'has-error' : 'has-success' }}">
@else
    <div class="form-group">
@endif
    {!! Form::label('category_id', 'Kelas:', ['class' => 'control-label']) !!}
    @if(count($category) > 0)
        {!! Form::select('category_id', $category, null, ['class' => 'form-control', 'id' => 'category_id', 'placeholder' => 'Pilih Kategori', 'required']) !!}
    @else
        <p>Tidak ada pilihan kategori</p>
    @endif
    @if ($errors->has('category_id'))
        <span class="help-block">{{ $errors->first('category_id') }}</span>
    @endif
</div>

@if ($errors->any())
    <div class="form-group {{ $errors->has('published_date') ? 'has-error' : 'has-success' }}">
@else
    <div class="form-group">
@endif
    {!! Form::label('published_date', 'Tanggal Terbit:', ['class' => 'control-label']) !!}
    {!! Form::date('published_date', !empty($book) ? $book->published_date : null, ['class' => 'form-control', 'id' => 'published_date', 'required']) !!}
    @if ($errors->has('published_date'))
        <span class="help-block">{{ $errors->first('published_date') }}</span>
    @endif
</div>

@if ($errors->any())
    <div class="form-group {{ $errors->has('synopsis') ? 'has-error' : 'has-success' }}">
@else
    <div class="form-group">
@endif
    {!! Form::label('synopsis', 'Sinopsis:', ['class' => 'control-label']) !!}
    {!! Form::textarea('synopsis', null, ['class' => 'form-control']) !!}
    @if ($errors->has('synopsis'))
        <span class="help-block">{{ $errors->first('synopsis') }}</span>
    @endif
</div>

@if ($errors->any())
    <div class="form-group {{ $errors->has('publisher') ? 'has-error' : 'has-success' }}">
@else
    <div class="form-group">
@endif
    {!! Form::label('publisher', 'Penerbit:', ['class' => 'control-label']) !!}
    {!! Form::text('publisher', null, ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('publisher'))
        <span class="help-block">{{ $errors->first('publisher') }}</span>
    @endif
</div>

@if ($errors->any())
    <div class="form-group {{ $errors->has('cover_photo') ? 'has-error' : 'has-success' }}">
@else
    <div class="form-group">
@endif
    {!! Form::label('cover_photo', 'Foto Cover:') !!}
    {!! Form::file('cover_photo') !!}
    @if ($errors->has('cover_photo'))
        <span class="help-block">{{ $errors->first('cover_photo') }}</span>
    @endif
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>