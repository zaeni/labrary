<div id="search">
{!! Form::open(['url' => 'book/search', 'method' => 'GET', 'id' => 'form-search']) !!}
    <div class="row">
        <div class="col-md-2">
            {!! Form::select('category_id', $category, (! empty($category_id) ? $category_id : null), ['id' => 'category_id', 'class' => 'form-control', 'placeholder' => '- Kategori -']) !!}
        </div>

        <div class="col-md-8">
            <div class="input-group">
            {!! Form:: text('keyword', (! empty($keyword)) ? $keyword : null,['class' => 'form-control', 'placeholder' => 'Masukkan Judul Buku']) !!}
            <span class="input-group-btn">
            {!! Form::button('Cari', ['class' => 'btn btn-default', 'type' => 'submit']) !!}
            </span>
            </div>
        </div>
    </div>
{!! Form::close() !!}
</div>