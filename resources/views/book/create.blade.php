@extends('template')

@section('main')
    <div id="book">
        <h2>Tambah Buku</h2>

        {!! Form::open(['url' => 'book', 'files' => true]) !!}
            @include('book.form', ['submitButtonText' => 'Tambah Buku'])
        {!! Form::close() !!}
    </div>
@stop