@extends('template')

@section('main')
    <div id="book">
        <h2>Data Buku</h2>

        @include('_partial.flash_message')

        @include('book.form_search')

        @if (count($books) > 0)
            <table class="table">
                <thead>
                    <tr>
                        <th>No. Buku</th>
                        <th>Judul</th>
                        <th>Kategori</th>
                        <th>Penerbit</th>
                        <th>Tanggal Terbit</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($books as $book)
                        <tr>
                            <td>{{ $book->book_number }}</td>
                            <td>{{ $book->title }}</td>
                            <td>{{ $book->category->category }}</td>
                            <td>{{ $book->publisher }}</td>
                            <td>{{ $book->published_date }}</td>
                            <td>
                                <div class="box-button">
                                    {{ link_to('book/' . $book->id, 'Detail', ['class' => 'btn btn-success btn-sm']) }}
                                </div>
                                @if (Auth::check())
                                    <div class="box-button">
                                        {{ link_to('book/' . $book->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
                                    </div>
                                    <div class="box-button">
                                        {!! Form::open(['method' => 'DELETE', 'action' => ['BookController@destroy', $book->id]]) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>Tidak ada data buku.</p>
        @endif

        <div class="table-nav">
            <div class="jumlah-data">
                <strong> Jumlah Buku: {{ $book_count }} </strong>
            </div>
            <div class="paging">
                {{ $books->links() }}
            </div>
        </div>

        @if (Auth::check())
            <div class="tombol-nav">
                <a href="book/create" class="btn btn-primary">Tambah Buku</a>
            </div>
        @endif
    </div> <!-- / #book -->
@stop