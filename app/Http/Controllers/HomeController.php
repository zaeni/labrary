<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $books = Book::paginate(4);
        $book_count = Book::count();
        $category = Category::pluck('category', 'id');
        return view('home', compact('books', 'book_count', 'category'));
    }

    public function detail($id)
    {
        $book = Book::where('id', $id)->first();
        return view('book.show', compact('book'));
    }
}
