<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use Illuminate\Http\Request;
use Session;
use Storage;
use Validator;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::paginate(5);
        $book_count = Book::count();
        $category = Category::pluck('category', 'id');
        return view('book.index', compact('books', 'book_count', 'category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::pluck('category', 'id');
        return view('book.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validate = Validator::make($input, [
            'title' => 'required',
            'category_id' => 'required',
            'book_number' => 'required',
        ]);

        if ($validate->passes()) {
            // Upload Foto
            if ($request->hasFile('cover_photo')) {
                $input['cover_photo'] = $this->uploadImage($request);
            }

            $buku = Book::create($input);

            Session::flash('flash_message', 'Data buku berhasil disimpan.');
        } else {
            return redirect()->back();
        }

        return redirect('book');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view('book.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $category = Category::pluck('category', 'id');
        return view('book.edit', compact('book', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $input = $request->all();

        // Cek apakah ada cover_photo baru di form?
        if ($request->hasFile('cover_photo')) {
            // Hapus cover_photo lama
            $this->deleteImage($book);

            // Upload cover_photo baru
           $input['cover_photo'] = $this->uploadImage($request);
        }

        $book->update($input);

        Session::flash('flash_message', 'Data buku berhasil diupdate.');

        return redirect('book');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $this->deleteImage($book);
        $book->delete();
        Session::flash('flash_message', 'Data buku berhasil dihapus.');
        Session::flash('penting', true);
        return redirect('book');
    }

    public function search(Request $request)
    {
        $keyword = trim($request->input('keyword'));
        $category_id = $request->input('category_id');

        if (!empty($keyword) || !empty($category_id)) {
            $category = Category::pluck('category', 'id');

            // Query
            $query = Book::where('title', 'LIKE', '%' . $keyword . '%');
            (! empty($category_id)) ? $query->Category($category_id) : '';
            $books = $query->paginate(2);

            // URL Links pagination
            $pagination = (! empty($category_id)) ? $pagination = $books->appends(['category_id' => $category_id]) : '';
            $pagination = $books->appends(['keyword' => $keyword]);

            $book_count = $books->total();
            return view('book.index', compact('books', 'keyword', 'pagination', 'book_count', 'category', 'category_id'));
        }

        return redirect('book');
    }

    private function uploadImage($request)
    {
        $cover_photo = $request->file('cover_photo');
        $ext  = $cover_photo->getClientOriginalExtension();

        if ($request->file('cover_photo')->isValid()) {
            $cover_photo_name   = date('YmdHis'). ".$ext";
            $upload_path = 'cover_photoupload';
            $request->file('cover_photo')->move($upload_path, $cover_photo_name);

            return $cover_photo_name;
        }
        return false;
    }

    private function deleteImage(Book $book)
    {
        $exist = Storage::disk('cover_photo')->exists($book->cover_photo);

        if (isset($book->cover_photo) && $exist) {
            $delete = Storage::disk('cover_photo')->delete($book->cover_photo);
            if ($delete) {
                return true;
            }
            return false;
        }
    }
}
