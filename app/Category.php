<?php

namespace App;

use App\Book;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'category'
    ];

    public function book()
    {
        return $this->hasMany(Book::class, 'category_id');
    }
}
