<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';

    protected $fillable = [
        'category_id',
        'book_number',
        'title',
        'publisher',
        'synopsis',
        'published_date',
        'cover_photo'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function scopeCategory($query, $category_id)
    {
        return $query->where('category_id', $category_id);
    }
}
