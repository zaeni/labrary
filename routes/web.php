<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/detail/{id}', 'HomeController@detail');

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::get('book/search', 'BookController@search');
Route::resource('book', 'BookController');
Route::resource('category', 'CategoryController');
Route::resource('user', 'UserController');