$(document).ready(function() {
    // Alert sliding.
    $('div.alert').not('.alert-important').delay(3000).slideUp(300);

    // Hapus Select dengan empty value dari URL.
    $("#form-search").submit(function() {
        $("#category_id option[value='']").attr("disabled","disabled");

        // Pastikan proses submit masih diteruskan.
        return true;
    });
});